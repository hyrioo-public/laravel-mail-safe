<?php

namespace Hyrioo\LaravelMailSafe;


class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        // Overrides default make mail command, to use a command that extends using ExtendedMailable
        // instead of the default mailable
        $this->app->extend('command.mail.make', function () {
            return new MakeMailCommandExtended($this->app['files']);
        });
    }
}
