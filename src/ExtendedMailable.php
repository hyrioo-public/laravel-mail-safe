<?php


namespace Hyrioo\LaravelMailSafe;


use Illuminate\Contracts\Mail\Mailer as MailerContract;
use Illuminate\Mail\Mailable;
use Illuminate\Support\Facades\App;

class ExtendedMailable extends Mailable
{
    /**
     * Prevents sending mail to disallowed domain if running outside production.
     * If know receivers are left after filtering the default dev mail is automatically added
     */
    protected function handleDevelopmentEnvironment()
    {
        if(!config('mail.enable_mail_filter')){
            return;
        }

        foreach ($this->to as $key => $to){
            $address = $to['address'];
            $domain_name = substr(strrchr($address, "@"), 1);
            if(!in_array($domain_name, config('mail.allowed_dev_mail_domains')) && !in_array($address, config('mail.allowed_dev_mails'))){
                info('Removing email receiver');
                unset($this->to[$key]);
            }
        }
        if(empty($this->to)){
            info('Added default dev email receiver');
            $this->to[] = config('mail.default_dev_mail');
        }
    }

    public function send(MailerContract $mailer)
    {
        $this->handleDevelopmentEnvironment();

        parent::send($mailer);
    }

    public function build()
    { }

    public static function preview(ExtendedMailable $mail, $receivers = null)
    {
        $mail->build();
        $mail->to = $receivers;
        return view($mail->view, $mail->buildViewData());
    }
}