# README #

This package ensures the receivers of a mail is filtered in non-production to the allowed domains and email-addresses.

The mailables must extend `Hyrioo\LaravelMailSafe\ExtendedMailable`, for the filter to automatically be applied.  
The package overrides the default `make:mail` command to make sure the mail generated is extending the correct class.  

Add the following keys to `config/mail.php`:
```
'enable_mail_filter' => env('MAIL_FILTER_ENABLED', false),
'allowed_dev_mail_domains' => [
  'example.com'
],
'allowed_dev_mails' => [
  'email@example.com'
],
'default_dev_mail' => [
  'name' => env('DEFAULT_DEV_MAIL_NAME', 'Developer'),
  'address' => env('DEFAULT_DEV_MAIL_ADDRESS', 'dev@example.com')
]
```
The `default_dev_mail` is used if all receivers has been filtered.  